#+OPTIONS: *:nil
** MOVE-TO (generic function)
 
: (NODE X Y &OPTIONAL Z)
Move the NODE to the point X,Y,Z.
