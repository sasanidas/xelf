#+OPTIONS: *:nil
** *PROJECT-DIRECTORIES* (variable)
List of directories where XELF will search for projects.
Directories are searched in list order.
