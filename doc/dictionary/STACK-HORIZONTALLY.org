#+OPTIONS: *:nil
** STACK-HORIZONTALLY (function)
 
: (&REST BUFFERS)
Combine BUFFERS into a single buffer, with each buffer stacked horizontally.
