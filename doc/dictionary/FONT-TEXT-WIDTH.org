#+OPTIONS: *:nil
** FONT-TEXT-WIDTH (function)
 
: (STRING &OPTIONAL (FONT *FONT*))
Width of STRING when rendered in FONT.
