#+OPTIONS: *:nil
** BOUNDING-BOX (generic function)
 
: (OBJECT)
Return the bounding-box of this OBJECT as multiple values.
The proper VALUES ordering is (TOP LEFT RIGHT BOTTOM), which could
also be written (Y X (+ X WIDTH) (+ Y HEIGHT)) if more convenient.
