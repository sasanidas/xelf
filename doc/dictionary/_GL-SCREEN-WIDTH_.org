#+OPTIONS: *:nil
** *GL-SCREEN-WIDTH* (variable)
Width of the window expressed in
OpenGL coordinates.
