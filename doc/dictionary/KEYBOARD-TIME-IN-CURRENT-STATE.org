#+OPTIONS: *:nil
** KEYBOARD-TIME-IN-CURRENT-STATE (function)
 
: (KEY)
Returns the duration in seconds that KEY is either pressed or
depressed.
