#+OPTIONS: *:nil
** QUADTREE-INSERT (generic function)
 
: (OBJECT TREE)
Insert the object OBJECT into the quadtree TREE.
