#+OPTIONS: *:nil
** BIND-EVENT (generic function)
 
: (NODE EVENT METHOD-NAME)
Bind the EVENT to invoke the method specified in METHOD-NAME, with
NODE as the sole argument to the method.

EVENT is a list of the form:

       (NAME modifiers...)

NAME is either a keyword symbol identifying the keyboard key, or a
string giving the Unicode character to be bound. MODIFIERS is a list
of keywords like :control, :alt, and so on.

Examples:
  
:  (bind-event self '(:up) 'move-up)
:  (bind-event self '(:down) 'move-down)
:  (bind-event self '(:q :control) 'quit)
:  (bind-event self '(:escape :shift) 'menu)

See also `keys.lisp' for the full table of key and modifier symbols.
