#+OPTIONS: *:nil
** MOVE-TOWARD (generic function)
 
: (NODE DIRECTION &OPTIONAL (STEPS))
Move the node STEPS units in compass direction
DIRECTION.
