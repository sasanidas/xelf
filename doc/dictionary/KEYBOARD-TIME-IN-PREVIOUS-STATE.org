#+OPTIONS: *:nil
** KEYBOARD-TIME-IN-PREVIOUS-STATE (function)
 
: (KEY)
Returns the duration in seconds that KEY was in its previous state
either pressed or depressed.
