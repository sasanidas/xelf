#+OPTIONS: *:nil
** *TIME-BASE* (variable)
Default time base. Don't set this
yourself; use SET-FRAME-RATE instead.
