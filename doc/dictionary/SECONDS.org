#+OPTIONS: *:nil
** SECONDS (function)
 
: (N)
Returns the number of updates in N seconds.
