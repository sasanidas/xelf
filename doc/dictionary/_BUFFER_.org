#+OPTIONS: *:nil
** *BUFFER* (variable)
The currently active buffer.
