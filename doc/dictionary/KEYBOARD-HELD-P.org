#+OPTIONS: *:nil
** KEYBOARD-HELD-P (function)
 
: (KEY)
Returns the duration in seconds that KEY has been depressed over a
number of game loops.
