#+OPTIONS: *:nil
** LOCATION (generic function)
 
: (NODE)
Return as values the X,Y location of NODE.
