#+OPTIONS: *:nil
** *EXECUTABLE* (variable)
Non-nil when running Xelf from a saved binary image.
