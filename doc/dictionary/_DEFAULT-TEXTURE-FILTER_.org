#+OPTIONS: *:nil
** *DEFAULT-TEXTURE-FILTER* (variable)
Filter used for drawing rendered outline fonts.
Valid values are :mipmap (the default), :linear, and :nearest.
