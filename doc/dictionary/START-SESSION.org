#+OPTIONS: *:nil
** START-SESSION (function)
Initialize the console, open a window, and play.
We want to process all inputs, update the game state, then update the
display.
