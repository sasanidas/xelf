#+OPTIONS: *:nil
** SWITCH-TO-BUFFER (function)
 
: (BUFFER)
Set the currently active buffer to BUFFER.
