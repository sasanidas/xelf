#+OPTIONS: *:nil
** *SCREEN-WIDTH* (variable)
Physical width of the window, in
pixels.
