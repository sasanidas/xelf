#+OPTIONS: *:nil
** STEP-IN-DIRECTION (function)
 
: (X Y DIRECTION &OPTIONAL (N 1))
Return the point X Y moved by n squares in DIRECTION.
