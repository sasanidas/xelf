#+OPTIONS: *:nil
** WITH-BORDER (function)
 
: (BORDER BUFFER)
Return a new buffer with the objects from BUFFER
surrounded by a border of thickness BORDER units.
