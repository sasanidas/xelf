#+OPTIONS: *:nil
** *JOYSTICK-DEVICE-NUMBER* (variable)
The number of the current joystick.
