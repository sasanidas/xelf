#+OPTIONS: *:nil
** RUN-HOOK (function)
 
: (HOOK)
Call all the functions in HOOK, in list order.
