#+OPTIONS: *:nil
** *BACKGROUND-COLOR* (variable)
The default background color of the XELF user interface.
