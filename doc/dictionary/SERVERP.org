#+OPTIONS: *:nil
** SERVERP (generic function)
 
: (BUFFER)
Return non-nil if this is the Netplay server.
