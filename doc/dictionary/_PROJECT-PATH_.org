#+OPTIONS: *:nil
** *PROJECT-PATH* (variable)
The pathname of the currently opened project.
