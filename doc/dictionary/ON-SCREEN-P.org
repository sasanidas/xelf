#+OPTIONS: *:nil
** ON-SCREEN-P (function)
 
: (NODE)
Return non-nil when NODE touches the buffer's window bounding box.
