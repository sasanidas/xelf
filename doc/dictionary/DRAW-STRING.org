#+OPTIONS: *:nil
** DRAW-STRING (function)
 
: (STRING X Y &KEY (COLOR *COLOR*) (FONT *FONT*) (Z 0))
Render the string STRING at x,y with color COLOR and font FONT.
