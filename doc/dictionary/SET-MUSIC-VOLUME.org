#+OPTIONS: *:nil
** SET-MUSIC-VOLUME (function)
 
: (NUMBER)
Set the mixer music volume between 0 (silent) and 127 (full volume).
