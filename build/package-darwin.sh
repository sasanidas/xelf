LISPDIR=/usr/local/bin
LISP=$LISPDIR/sbcl

NAME="danceball"

rm -rf $NAME.app
$LISP --no-sysinit --no-userinit --load bootslime.lisp

cp -a Frameworks $NAME.app/Contents/
mkdir -p $NAME.app/Contents/Resources/projects/danceball
cp -a xelf/standard $NAME.app/Contents/Resources/projects/
cp danceball/COPYING danceball/*.{wav,png,ogg,txt,ttf,otf} $NAME.app/Contents/Resources/projects/danceball
mkdir -p $NAME.app/Contents/Source
cp danceball/*.lisp danceball/*.asd xelf/*.lisp xelf/*.asd $NAME.app/Contents/Source
mkdir -p $NAME.app/Contents/Licenses
cp xelf/licenses/* $NAME.app/Contents/Licenses

