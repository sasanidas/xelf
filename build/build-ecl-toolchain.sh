# adapted from ecl-android build and install instructions
# needs NDK9: http://dl.google.com/android/ndk/android-ndk-r9-linux-x86.tar.bz2

export PLATFORM_PREFIX=/opt/toolchains/android-ndk/
export NDK_PATH=/opt/android-ndk/
export NDK_PLATFORM=android-4

cd ~/src/ecl

./configure ABI=32 CFLAGS="-m32 -g -O2" LDFLAGS="-m32 -g -O2" \
            --disable-longdouble \
            --prefix=`pwd`/ecl-android-host \
            --enable-libatomic=included

make && make install
export ECL_TO_RUN=`pwd`/ecl-android-host/bin/ecl
rm -r build

rm -rf ${PLATFORM_PREFIX}

mkdir -p ${PLATFORM_PREFIX}
${NDK_PATH}/build/tools/make-standalone-toolchain.sh \
           --platform=${NDK_PLATFORM} \
           --install-dir=${PLATFORM_PREFIX} \
           --arch=arm

export PATH=${PLATFORM_PREFIX}/bin:${PATH}

./configure --host=arm-linux-androideabi \
	    --with-dffi \
            --prefix=`pwd`/ecl-android \
            --with-cross-config=`pwd`/src/util/android.cross_config \
            --disable-soname

make -j9
make install

cd ~/src

