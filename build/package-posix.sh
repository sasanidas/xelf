#!/bin/sh

cd ~/release
git clone https://gitlab.com/dto/xelf.git
mv xelf danceball-0.7
cd danceball-0.7
git clone https://gitlab.com/dto/danceball.git
rm -rf .git
rm -rf danceball/.git
cp ~/danceball/danceball.bin .
cd ~/release
tar cvzf danceball-0.7.tar.gz danceball-0.7


# cp -R danceball-linux-0.6 danceball-windows-0.6
# cd danceball-windows-0.6
# rm *.bin
# cp ~/xelf/danceball.exe .
# cp ~/xelf/*.dll .
# cd ..
# zip -r danceball-windows-0.6.zip danceball-windows-0.6


