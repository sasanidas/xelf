# needs java 8

export PLATFORM_PREFIX=/opt/toolchains/android-ndk/
export NDK_PATH=/opt/android-ndk/
export NDK_PLATFORM=android-4
export PATH=${PLATFORM_PREFIX}/bin:${PATH}

cd ~/src/ecl-android
/opt/android-sdk-linux/tools/android update project -t android-10 -p .
ln -s ~/src/ecl/ecl-android ecl-android
ln -s ecl-android/lib/ecl-16.1.2 ecl-libdir
/opt/android-ndk/ndk-build 
ant debug

cd ~/src
