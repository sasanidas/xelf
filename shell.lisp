;;; shell.lisp --- command shell for xelf

;; Copyright (C) 2006-2017  David O'Toole

;; Author: David O'Toole <deeteeoh1138@gmail.com>
;; Keywords: games, gui

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public License
;; as published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; Package declaration

(in-package :xelf)

;;; Messenger widget

(defclass messenger (node)
  ((category :initform :terminal)
   (messages :initform nil)))

(defmethod initialize-instance :after ((self messenger) &key messages) 
  (typecase messages
    ((stringp messages)
     (setf (slot-value self 'messages) (list messages)))
    ((consp messages)
     (setf (slot-value self 'messages) messages))))

(defmethod add-message ((self messenger) message-string)
  (assert (stringp message-string))
  (push message-string (slot-value self 'messages)))

(defparameter *messenger-columns* 80)
(defparameter *messenger-rows* 7)

(defmethod get-messages ((self messenger))
  (or (slot-value self 'messages) *message-history*))

(defmethod layout ((self messenger))
  (setf (slot-value self 'height) (+ (* (font-height *font*) *messenger-rows*)
		   (dash 4)))
  (let ((width 0))
    (block measuring
      (dotimes (n *messenger-rows*)
	(if (<= (length (get-messages self)) n)
	    (return-from measuring nil)
	    (setf width 
		  (max width 
		       (font-text-width 
			(nth n (get-messages self))
			*block-font*))))))
    (setf (slot-value self 'width) (+ width (dash 5)))))

(defparameter *messenger-color* "gray80")

(defmethod draw ((self messenger))
  (draw-background self)
  (with-slots (x y width height) self
      (let ((y0 (+ y height (- 0 (font-height *font*) (dash 2))))
	    (x0 (+ x (dash 3))))
	(dotimes (n *messenger-rows*)
	  (unless (<= (length (get-messages self)) n)
	    (draw-string (nth n (get-messages self))
			 x0 y0
			 :color *messenger-color*
			 :font *block-font*)
	    (decf y0 (font-height *font*)))))))

;;; Modeline

(defvar *modeline-status-string* nil)

   (defun show-status (string)
     (setf *modeline-status-string* (concatenate 'string "        " (clean-string string))))

   (defun modeline-status-string ()
     (if *notification*
         (get-first-line *notification*)
	 *modeline-status-string*))

   (defun-memo modeline-position-string (x y)
       (:key #'identity :test 'equal :validator #'identity)
     (format nil "X:~S Y:~S" x y))

   (defun-memo modeline-database-string (selected local global)
       (:key #'identity :test 'equal :validator #'identity)
     (format nil "~S objects selected from ~S/~S objects" selected local global))

   (define-visual-macro modeline
       (:super phrase
	:slots 
	((orientation :initform :horizontal)
	 (no-background :initform t)
	 (spacing :initform 4))
	:inputs (:buffer-id (make-instance 'label :read-only t)
		 :position (make-instance 'label :read-only t)
		 :mode (make-instance 'label :read-only t)
		 :objects (make-instance 'label :read-only t)
		 :status (make-instance 'label :read-only t))))

   (defmethod update ((self modeline))
     (mapc #'pin (slot-value self 'inputs))
     (with-visual-slots (buffer-id objects position mode status) self
       (set-value buffer-id (slot-value (current-buffer) 'buffer-name))
       (set-value objects (modeline-database-string
			   (length (selection))
			   (hash-table-count (slot-value (current-buffer) 'objects))
			   (hash-table-count *database*)))
       (set-value position
		  (modeline-position-string
		   (slot-value (current-buffer) 'window-x)
		   (slot-value (current-buffer) 'window-y)))
       (set-value mode
		  (if (current-buffer)
		      (if (slot-value (current-buffer) 'paused)
			  "(paused)"
			  "(playing)")
		      "(empty)"))
       (set-value status
		  (or (modeline-status-string) " "))))
   
   (defmethod draw ((self modeline))
     (with-slots (x y width height) self
       (call-next-method)
       (draw-line x y (+ x width) y :color "gray50")))

;;; Interactive dialog box tools

(defun arglist-input-forms (argument-forms)
  (mapcar #'(lambda (f)
	      `(make-sentence 
		(list
		 (make-instance 'pretty-symbol-entry :value ,(make-keyword (first f)) :read-only t)
		 (make-instance 'property-value-entry :value ,(second f) :read-only nil))))
	  argument-forms))

(defun command-inputs (name arglist)
  `(;;(let ((label (make-instance 'label :read-only t :font "sans-condensed-bold-18")))
    ;;  (prog1 label (set-value label ,(command-name-string (symbol-name name)))))
    (make-paragraph (list ,@(arglist-input-forms arglist)))))

(defun command-name-string (thing)
  (let ((name (etypecase thing
		(symbol (symbol-name thing))
		(string thing))))
    (coerce 
     (string-capitalize 
      (substitute #\Space #\- 
		  (string-trim " " name)))
     'simple-string)))

(defun command-argument-string (thing)
  (concatenate 'string (command-name-string thing) ": "))

(defun action-name (name)
  (intern (concatenate 'string (symbol-name name) "-ACTION")))

(defun show-name (name)
  (intern (concatenate 'string "SHOW-" (symbol-name name) "-DIALOG")))

(defun dialog-class-name (name)
  (intern (concatenate 'string (symbol-name name) "-DIALOG")))

;;; Shell prompt

(defclass shell-prompt (entry)
    ((result :initform nil)
     (background :initform nil)
     (history :initform nil)))

  (defmethod make-halo ((self shell-prompt))
    nil)

  (defmethod can-pick ((self shell-prompt)) nil)

  (defmethod pick ((self shell-prompt))
    nil)

  (defmethod evaluate-expression ((self shell-prompt) sexp)
    (let ((*interactive-p* t))
      (with-slots (result) self 
        (setf result (eval sexp)))))

  (defmethod enter :after ((self shell-prompt) &optional no-clear)
    (with-slots (result error-output) self
      (if error-output
	  (progn
	    (replace-output (shell) (list (make-phrase (clean-string error-output))))
	    (notify "There was an error. Check the output area for more info."))
	  (when result
	    (replace-output (shell) (list (make-phrase result))))))
    (clear-line self))
    
  (defmethod lose-focus ((self shell-prompt))
    (cancel-editing self))

  ;; (defmethod close-shell ((self shell-prompt))
  ;;   (setf (shell-p (current-buffer)) nil))

  ;; (defmethod initialize-instance :after ((self shell-prompt) &key)
  ;;   (bind-event self '(:g :control) 'close-shell)
  ;;   (bind-event self '(:escape) 'close-shell))

(defparameter *minimum-shell-width* 400)
(defparameter *shell-background-color* "gray20")

(defparameter *default-command-prompt-string* " > ")

(defun make-label (string &optional font)
  (let ((label (make-instance 'label)))
    (prog1 label
      (set-value label string)
      (set-read-only label t)
      (when font
	(setf (slot-value label 'font) font)))))

(define-visual-macro shell
    (:super (phrase traveler)
	    :slots 
	    ((orientation :initform :vertical)
	     (frozen :initform t)
	     (category :initform :system)
	     (spacing :initform 4)
	     ;;
	     (entry-index :initform 0)
	     (target-x :initform 0)
	     (target-y :initform 0))
	    :inputs
	    (:output (make-instance 'phrase)
		     :modeline (make-instance 'modeline)
		     :command-area (make-sentence 
				    (list
				     (make-label *default-command-prompt-string*)
				     (make-instance 'shell-prompt)))))
  (at-next-update (evaluate-expression (shell-prompt) (list 'in-package (package-name (project-package)))))
  (setf *menubar* (make-instance 'menubar))
  (setf *system* (make-instance 'system)))

(defmethod draw :after ((self shell))
  (let ((focus (slot-value (current-buffer) 'focused-block)))
    (when (xelfp focus)
      (draw-focus (find-object focus)))))

(defun create-shell-maybe ()
  (when (null *shell*) 
    (setf *shell* (make-instance 'shell))))

;;; shell.lisp ends here
