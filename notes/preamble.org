
* About this document 
 
This is the work-in-progress documentation for the Xelf game engine. 
Xelf stands for "extensible emacs-like facility". You can create 
active game objects called "nodes"; nodes can move and interact by 
being grouped into "buffers". Node and buffer classes can be 
subclassed to implement game logic and override default behavior. 
Other features include efficient quadtree-based collision detection, 
A-star pathfinding, gamepad support, and more. 
 
 - Xelf [[http://gitlab.com/dto/xelf][source code repository]] at gitlab 
 - Xelf home page: http://xelf.me/ 
 - Plong game example https://gitlab.com/dto/plong 
 - Plong tutorial page http://xelf.me/plong.html
 - 3x0ng (larger game) [[http://gitlab.com/dto/3x0ng][source code repository]] at gitlab 
 - 3x0ng home page: http://xelf.me/3x0ng.html 
 - 1x0ng (other game) [[http://gitlab.com/dto/1x0ng][source code repository]] at gitlab 
 
Known issues: 
 
 - Some things are not documented yet. 
 
* About Common Lisp 
 
This document assumes that the user can use a command shell and a Lisp 
system. Using Xelf effectively will also require familiarity with 
Common Lisp development in general, and CLOS in particular. 
 
Teaching Common Lisp is outside the scope of this document. The 
[[http://en.wikipedia.org/wiki/Common_Lisp][wikipedia page for Common Lisp]] has a reasonable capsule explanation of 
the syntax, with links to further resources. There's also Zach Beane's 
[[http://xach.livejournal.com/325276.html]["Where to get help with Common Lisp"]]. 
 
** Handling Lisp in your text editor  
  
If you clicked the link in the last section to the Wikipedia page on  
Common Lisp and looked at the code examples, you probably noticed all  
the parentheses. The heavy use of parentheses marks Lisp off from  
other languages and makes it feel unfamiliar. But once you are able to  
read and write the basics, it all falls together naturally. The  
unusual syntax of Lisp is actually the key to many of its coolest  
features.  
  
Matching up parentheses and indenting things properly is best handled  
automatically by a text editor with support for editing Lisp code,  
such as [[http://www.vim.org][Vim]], which can be used with [[http://www.vim.org/scripts/script.php?script_id=2531][Slimv]] --- the Superior Lisp  
Interaction Mode for Vim. I myself use the [[http://www.gnu.org/software/emacs][GNU Emacs]] text editor along  
with [[http://common-lisp.net/project/slime/][Slime]].   
  
Using one of these options---Emacs with Slime, or Vim with Slimv---is  
probably the best way to develop and test Lisp code made with  
Xelf. With Slime or Slimv you can edit Lisp code efficiently, send  
commands to the underlying Lisp environment that Xelf is running in,  
or redefine methods and functions in order to alter object behaviors  
while the system is running.   
  
Both Emacs and Vim are highly customizable development environments,  
not just text editors; in fact, I have developed and tested Xelf  
(all ~8KLOC) entirely with GNU Emacs as my [[http://en.wikipedia.org/wiki/Integrated_development_environment][IDE.]]  
  
Furthermore, Emacs and Vim are [[http://en.wikipedia.org/wiki/Free_software][Free software]], will run on basically  
any platform, are of very high quality, and have large, friendly user  
communities.  
  
That being said, you can edit Lisp code in basically any text editor,  
and it's quite possible that the text editor you already use has a  
plugin or script available for editing Lisp code and matching those  
parentheses. If you're unsure about Vim and Emacs, try looking around  
to see if you can find Lisp support for your existing editor.  

The instructions below assume Slime is being used.
 
* Getting Started with Xelf 
 
** 1. Install SDL 1.2 libraries 
 
If you are on GNU/Linux, you must install certain libraries through
your distributions' software installer: SDL 1.2, SDL-IMAGE, SDL-MIXER,
SDL-GFX, AND SDL-TTF. You may additionally need to install the
corresponding SDL-*-DEV versions of those packages.

The exact package names vary between distributions; you may wish to
use Synaptic to search and find the right ones. Remember that Xelf
uses SDL 1.2 and not SDL2.

Windows users can find the relevant SDL 1.2 DLL's in the Xelf
distribution, in ./xelf/contrib/win32/

** 2. Install GNU Emacs and SBCL

On Linux, installing GNU Emacs could be as simple as:

: sudo apt-get install emacs 

or you may wish to use Synaptic.
 
On Windows you will need to visit the [[http://www.gnu.org/software/emacs][GNU Emacs site]].

Although Linux package managers often have SBCL, it can be a very old
version. Instead you can download the newest stable version from [[http://sbcl.org][SBCL.org]].

** 3. Install Quicklisp 
     
 - http://www.quicklisp.org/  
 
It takes only a few moments to install Quicklisp, and this is the best 
way to download and install all the Common Lisp libraries that Xelf 
depends on---automatically. 

To begin, download https://beta.quicklisp.org/quicklisp.lisp
and load it with SBCL at the shell:

 : sbcl --load quicklisp.lisp

Read the text that comes up and you'll be prompted to enter this:

:  (quicklisp-quickstart:install)

After installing quicklisp you will see a notice about adding 
Quicklisp to your Lisp startup file with (ql:add-to-init-file). 
 
Doing this will make Quicklisp load automatically when you start 
your Lisp implementation, but it isn't strictly required. 
    
:  (ql:add-to-init-file)

** 4. Install Slime

Start SBCL and enter the following:

: (ql:quickload :quicklisp-slime-helper)

Afterward, you should set up your Emacs to load this Slime as
mentioned here:

 - https://github.com/quicklisp/quicklisp-slime-helper
 
In short, add this to your Emacs init file:

:  (load (expand-file-name "~/quicklisp/slime-helper.el"))
:  (setq inferior-lisp-program "sbcl")

To start slime from within Emacs, hit ALT-X and then type "slime" and
hit RETURN/ENTER.

For more help on how to configure and start Slime, see the Slime User Manual:

 - https://common-lisp.net/project/slime/doc/html/index.html

** 5. Download Xelf 
   
:   cd ~/quicklisp/local-projects 
:   git clone https://gitlab.com/dto/xelf.git
:   git clone https://gitlab.com/dto/quadrille.git

** 6. Load Xelf

Once Emacs and Slime are started, load and compile Xelf from the REPL:

:   (ql:quickload :xelf)
 
** 7. Run the example game 
 
At the shell:  
 
:   cd ~/quicklisp/local-projects
:   git clone https://gitlab.com/dto/plong.git
 
Then in Slime: 
       
:   (ql:quickload :plong) 
:   (plong:plong) 
 
If this works, you're ready to start working with Xelf.  If not, 
please report problems to me at [[mailto:dto@xelf.me][dto@xelf.me]]. You can also visit the 
channel "#lispgames" on irc.freenode.org. 

You can substitute "3x0ng" or "1x0ng" instead of "plong".

** 8. Making a new project

This section is under construction. In the meantime you can use
Quickproject to get started, and then use the Plong example as a
boilerplate.

:  (ql:quickload :quickproject)

See http://www.xach.com/lisp/quickproject/ for documentation on
Quickproject. In short:

: (quickproject:make-project #p"~/src/myproject/" :depends-on '(xelf))

In the generated "package.lisp" you will need to add Xelf to the list
of packages being used:

:   (defpackage #:myproject
:     (:use #:cl #:xelf))

In the future this will be handled automatically.

** 9. Read the tutorial

If you're ready to move on, [[http://xelf.me/plong.html][visit the tutorial page.]]

** 10. (Optional) Install Draft ANSI Common Lisp Standard

This will make it easy to dynamically look up definitions of Common
Lisp symbols and read the documentation as hypertext within Emacs.

First you will need to download and extract the files formatted for
GNU Emacs. ftp://ftp.ma.utexas.edu/pub/gcl/gcl-info+texi.tgz

Then add the following to your Emacs initialization file: 
 
: (require 'info-look)
: (add-to-list 'Info-directory-list (file-name-as-directory "/home/dto/gcl-info/"))
: (setq Info-default-directory-list (cons "/usr/local/info/"  Info-default-directory-list))
: (info-lookup-add-help
:  :mode 'lisp-mode
:  :regexp "[^][()'\" \t\n]+"
:  :ignore-case t
:  :doc-spec '(("(gcl.info)Symbol Index" nil nil nil)))
:
: (add-to-list 'load-path (expand-file-name "~/gcl-info/"))
: (require 'get-gcl-info)

** 11. (Optional) Delivering executables of your game

Please see my [[https://gitlab.com/dto/xelf/blob/master/build/build-it.lisp][notes on building and cross-compilation]]. The build
script will work on both Linux and Windows, and you can even use Wine
to cross-compile self-contained Win32 downloadables from within Linux!

A Mac OSX version of these build scripts is in the works. You can
check out the current [[https://gitlab.com/dto/xelf/blob/master/build/mac-notes.txt][work-in-progress document]] to learn more. 
